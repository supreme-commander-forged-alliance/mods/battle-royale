
-- LOBBY OPTIONS --

--- Options received from the lobby through the sim.
Config = { }

-- CARE PACKAGES --

CarePackage = { }
CarePackage.Coordinates = nil
CarePackage.InWater = false
CarePackage.Blueprints = { }
CarePackage.Interval = 40
CarePackage.Timestamp = 0

-- SHRINKING --

Shrink = { }
Shrink.CurrentArea = { 0, 0, 512, 512 }
Shrink.NextArea = { 0, 0, 512, 512 }
Shrink.Interval = 40
Shrink.Timestamp = 0
