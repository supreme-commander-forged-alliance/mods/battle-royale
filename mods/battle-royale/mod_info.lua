name = "Battle Royale"
version = 6
uid = "battle-royale-06"

copyright = "GNU-3"
description = "This mod requires the UI mod Dear Windowing. Without it you will have no UI. A battle royale mod including (free of charge) care packages!"
author = "(Jip) Willem Wijnia"
url = "https://gitlab.com/supreme-commander-forged-alliance/mods"

-- <div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>               title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
icon = "/mods/battle-royale/icon.png"
selectable = true
enabled = true
exclusive = false
ui_only = false
requires = { }
requiresNames = { }
conflicts = { }
before = { }
after = { }
